// [SECTION] Document Object Model
	// allow us to be able to access or modify the properties of an html element in a webpage
	// it is a standard on how to get, change, add or delete HTML elements
	// we will focus on using DOM for managing forms.

	// For selecting HTML elements will be using the document.querySelector("#")

	// Syntax: document.querySelector("htmlelement")
		//"documents" refers to the whole page
		// ".querySelector" is used to select a specific object (HTML elements) from the web page
		// the query selector function takes a spring input that is formatted like a CSS selector when applying styles.

	const txtFirstName = document.querySelector("#txt-first-name");
	const txtLastName = document.querySelector("#txt-last-name");
	const spanFullName = document.querySelector("#span-full-name");
// [SECTION] Event Listeners
	// Whenever a user interacts with a webpage, this action is considered as an event
	// Working with evetns is a large part of creating interactivity in a webpage
	// to perform an action when an event triggers, you first need to listen to it.

	// the method use is "addEventListener" that takes two arguments:
		// A string identifying an event;
		// and a function that the listener will execute once the "specified event is triggered"
		// When an event occurs, an "event obbject" is passed to the function argument as the first parameter

	// txtFirstName.addEventListener("keyup",(event) => {
	// 	console.log(event.target); // contains the element
	// 	console.log(event.target.value); // contains the actual value
	// });

	txtFirstName.addEventListener("keyup", () => {

		// The "innerHTML" property sets or return the HTML content (inner HTML) of an element(div,span, etc.)
		// the ".value" property sets or returns the value of an attribute. (form control elements: input, select, etc)
		spanFullName.innerHTML = `${txtFirstName.value}`;
	});

	// Creating Multiple events that will trigger a same function
	
	const fullName = () => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	}

	txtFirstName.addEventListener("keyup", fullName);
	txtLastName.addEventListener("keyup", fullName);

	//Activity s47
    /*
    1. Create an addEventListener that will "change the color" of the "spanFullName". 
    2. Add a "select tag" element with the options/values of red, green, and blue in your index.html.
    3. The values of the select tag will be use to change the color of the span with the id "spanFullName"

    Check the following links to solve this activity:
        HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
        HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

    */

   // My Solution:

    const changeFullNameColor = document.querySelector("#text-color")

    changeFullNameColor.addEventListener("change", () => {
    	spanFullName.style.color = changeFullNameColor.value;
    })


